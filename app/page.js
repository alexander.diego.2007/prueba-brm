"use client";
import React, { useState, useEffect } from "react";

export default function Home() {
  const [modal, setModal] = useState(false);
  const [value, setValue] = useState([]);
  const [data, setData] = useState([]);
  const [edit, setEdit] = useState(false);
  const [index, setIndex] = useState(0);

  const saveData = () => {
    setModal(false);
    data.push(value);
  };

  const editItem = (
    valueName,
    valuePhone,
    valueDate,
    valueAddress,
    valueEmail,
    valueEdad
  ) => {
    setModal(false);
    setEdit(false);
    data[index].name = valueName;
    data[index].phone = valuePhone;
    data[index].date = valueDate;
    data[index].address = valueAddress;
    data[index].email = valueEmail;
    data[index].edad = valueEdad;

    return data;
  };

  const showEdit = (
    valueName,
    valuePhone,
    valueDate,
    valueAddress,
    valueEmail,
    valueEdad
  ) => {
    data[index].name = valueName;
    data[index].phone = valuePhone;
    data[index].date = valueDate;
    data[index].address = valueAddress;
    data[index].email = valueEmail;
    data[index].edad = valueEdad;
  };

  const deleteItem = (itemA) => {
    var index = data
      .map((item) => {
        return item.name;
      })
      .indexOf(itemA);
    data.splice(index, 1);
    return data;
  };

  useEffect(() => {
    const productsArray = [];
    data.forEach((child) => {
      const productElement = {
        name: child.name,
        phone: child.phone,
        date: child.date,
        address: child.address,
        email: child.email,
        edad: child.edad,
      };
      productsArray.push(productElement);
    });
    setData(productsArray);
  });

  return (
    <div className="content_data">
      <table>
        <tr>
          {[
            "Nombre",
            "Teléfono",
            "Fecha de nacimiento",
            "Dirección",
            "Correo electrónico",
            "Edad",
          ].map((item) => {
            return <th>{item}</th>;
          })}
          <th></th>
          <th></th>
        </tr>
        {data.map((item, index) => {
          return (
            <tr>
              <td>{item.name}</td>
              <td>{item.phone}</td>
              <td>{item.date}</td>
              <td>{item.address}</td>
              <td>{item.email}</td>
              <td>{item.edad}</td>
              <td>
                <button
                  className="py-2.5 px-5 mr-2 mb-2 text-sm font-medium text-gray-900 focus:outline-none bg-white rounded-lg border border-gray-200 hover:bg-gray-100 hover:text-blue-700 focus:z-10 focus:ring-4 focus:ring-gray-200 dark:focus:ring-gray-700 dark:bg-gray-800 dark:text-gray-400 dark:border-gray-600 dark:hover:text-white dark:hover:bg-gray-700"
                  onClick={() => {
                    setModal(true);
                    setEdit(true);
                    setIndex(index);
                  }}
                >
                  Editar
                </button>
              </td>
              <td>
                <button
                  className="focus:outline-none text-white bg-red-700 hover:bg-red-800 focus:ring-4 focus:ring-red-300 font-medium rounded-lg text-sm px-5 py-2.5 mr-2 mb-2 dark:bg-red-600 dark:hover:bg-red-700 dark:focus:ring-red-900"
                  onClick={() => {
                    edit ? null : deleteItem(item.name);
                  }}
                >
                  Eliminar
                </button>
              </td>
            </tr>
          );
        })}
      </table>
      <button
        className="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 mr-2 mb-2 dark:bg-blue-600 dark:hover:bg-blue-700 focus:outline-none dark:focus:ring-blue-800"
        onClick={() => setModal(true)}
      >
        Crear nuevo
      </button>
      {modal && (
        <div>
          <table>
            <tr>
              {[
                "Nombre",
                "Teléfono",
                "Fecha de nacimiento",
                "Dirección",
                "Correo electrónico",
                "Edad",
              ].map((item) => {
                return <th>{item}</th>;
              })}
              <th></th>
              <th></th>
            </tr>
            <tr>
              {[
                { id: 1, name: "name", description: "Nombre" },
                { id: 2, name: "phone", description: "Teléfono" },
                { id: 3, name: "date", description: "Fecha de nacimiento" },
                { id: 4, name: "address", description: "Dirección" },
                { id: 5, name: "email", description: "Correo electrónico" },
                { id: 6, name: "edad", description: "Edad" },
              ].map((item) => {
                return (
                  <th>
                    <input
                      name={item.name}
                      onChange={(e) =>
                        setValue({ ...value, [e.target.name]: e.target.value })
                      }
                      value={
                        edit
                          ? item.name === "name"
                            ? data[index]?.name !== value["name"]
                              ? value["name"]
                              : data[index]?.name
                            : item.name === "phone"
                            ? data[index]?.phone !== value["phone"]
                              ? value["phone"]
                              : data[index]?.phone
                            : item.name === "date"
                            ? data[index]?.date !== value["date"]
                              ? value["date"]
                              : data[index]?.date
                            : item.name === "address"
                            ? data[index]?.address !== value["address"]
                              ? value["address"]
                              : data[index]?.address
                            : item.name === "email"
                            ? data[index]?.email !== value["email"]
                              ? value["email"]
                              : data[index]?.email
                            : item.name === "edad"
                            ? data[index]?.edad !== value["edad"]
                              ? value["edad"]
                              : data[index]?.edad
                            : value[item.edad]
                          : value[index]
                      }
                      type={item.name === "date" ? "date" : "text"}
                    />
                  </th>
                );
              })}
              <th>
                <button
                  className="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 mr-2 mb-2 dark:bg-blue-600 dark:hover:bg-blue-700 focus:outline-none dark:focus:ring-blue-800"
                  onClick={(e) => {
                    edit
                      ? editItem(
                          value["name"],
                          value["phone"],
                          value["date"],
                          value["address"],
                          value["email"],
                          value["edad"]
                        )
                      : saveData(
                          value["name"],
                          value["phone"],
                          value["date"],
                          value["address"],
                          value["email"],
                          value["edad"]
                        );
                  }}
                >
                  {edit ? "Editar" : "Crear nuevo"}
                </button>
              </th>
            </tr>
          </table>
        </div>
      )}
    </div>
  );
}
